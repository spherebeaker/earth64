import hashlib
import base58
import binascii
from earth64 import config

class TransactionEncoderService:
    PREFIX = "00"

    @classmethod
    def validate_bitcoin_address(cls, address):
        #address = "1BvBMSEYstWetqTFn5Au4m4GFg7xJaNVN2"

        base58_decoded = base58.b58decode(address).hex()
        prefix_and_hash = base58_decoded[:len(base58_decoded)-8]
        checksum = base58_decoded[len(base58_decoded)-8:]
        hash = prefix_and_hash
        for x in range(1,3):
            hash = hashlib.sha256(binascii.unhexlify(hash)).hexdigest()
      
        if(checksum == hash[:8]):
            print("[TRUE] checksum is valid!")
        else:
            print("[FALSE] checksum is not valid!")

    @classmethod
    def hash_to_address(cls, hash_hex):
        prefix_and_hash = cls.PREFIX+hash_hex[:40]
        a_hash = prefix_and_hash
        for x in range(1,3):
            a_hash = hashlib.sha256(binascii.unhexlify(a_hash)).hexdigest()
        checksum_hex = a_hash[:8]
        address_hex = prefix_and_hash+checksum_hex
        address = base58.b58encode(binascii.unhexlify(address_hex))
        #cls.validate_bitcoin_address(address)
        return address

    @classmethod
    def encode_message_cli(cls, filename):
        print("\nMaster public key:")
        print(config.get_pub_key(1))

        print("\nAddress for encoded message: ")
        message = open(filename).read()
        print(cls.encode_message(message).decode("utf-8"))

    @classmethod
    def encode_message(cls, message):
        encoded = hashlib.sha256(message.encode('utf-8')).hexdigest()
        return cls.hash_to_address(encoded)

    @classmethod
    def help_encode_message(cls):
        print("TODO")


    @classmethod
    def validate_message_cli(cls, filename, address):
        message = open(filename).read()
        res = cls.validate_message(message, address)
        if res:
            print("\nMessage is valid")
        else:
            print("\nMessage it NOT valid")

    @classmethod
    def validate_message(cls, message, address):
        hash_address = cls.encode_message(message).decode("utf-8")
        
        if address == hash_address:
            return True
        return False

    @classmethod
    def help_validate_message(cls):
        print("TODO")


def main():
    message = "dsss"
    res = TransactionEncoder.encode_transaction(message)
    print(res)

if __name__=='__main__':
    main()
