from earth64 import config
from bip32 import HARDENED_INDEX
from coinaddress import address_from_xpub

class AccountService:
    @classmethod
    def help(cls):
        print("TODO")
        pass

    @classmethod
    def show(cls):
        print("Root key: "+config.root_public_key)
        print("Master seed: ", config.seed_key)
        print("Public Keys:")
        for i in range(1, 10):
            print(" "+str(i)+".)", config.get_pub_key(i))
        
        pass

    @classmethod
    def get_address_from_node_path(cls, path_number):
        return config.get_pub_key(path_number)
     
        
    @classmethod
    def set_encoded_address(cls, address):
        config.set_master_address(address)
        print("\nEncoded address set to "+address)

    @classmethod
    def help_set_encoded_address(cls):
        print("TODO")

   
