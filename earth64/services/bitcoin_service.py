import http.client
import json
import time

last_hash = ""
last_hash_time = 0

class BitcoinService:
    @classmethod
    def get_latest_block_hash(cls):
        global last_hash
        global last_hash_time 
        if time.time()-last_hash_time > 180:
            url = "https://chain.api.btc.com/v3/block/latest"
            conn = http.client.HTTPSConnection("chain.api.btc.com")
            conn.request("GET", "/v3/block/latest")
            response = conn.getresponse()
            data = response.read()
            res = json.loads(data)
            h = res['data']['hash']
            last_hash = h 
            last_hash_time = time.time()
        else:
            h = last_hash
        return h

def main():
    BitcoinService.get_latest_block_hash()

if __name__=='__main__':
    main()
