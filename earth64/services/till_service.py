import os
import os.path
import errors
from earth64.services.bitcoin_service import BitcoinService
import json
from earth64.errors import FileException, BlockException
from earth64 import config
import hashlib
import shutil
import time

class TillService:
    @classmethod
    def till(cls, prefix="", limit="0"):
        if not config.master_address:
            print("\nMust set the encoded message address before tilling")
            return
        try:
            cls.create_master_file()
        except:
            pass
        limit = int(limit, 10)
        print("Creating files...")
        last_time = 0
        current_path_number = config.current_path_number
        curr_limit = 0
        last_file = ""
        while True:
            curr_length = len(bin(curr_limit)[2:])
            if limit and curr_length > limit:
                break
            curr_limit += 1
            #print(current_path_number, curr_length, limit)
            if current_path_number < 1:
                current_path_number+=1
            else:
                current_path_string = bin(current_path_number)[2:]
                if not prefix or current_path_string.startswith(prefix):
                    try:
                        #print("splitting for ", current_path_number)
                        cls.split(current_path_number)
                        last_file = current_path_number
                        if time.time()-last_time > 10:
                            last_time = time.time()
                            print("File creation number: "+str(current_path_number))
                        current_path_number+=1   
                    except KeyboardInterrupt:
                        raise
                    except:
                        import traceback
                        traceback.print_exc()
                        pass
                else:
                    current_path_number+=1
                    continue
            config.current_path_number = current_path_number
            config.save_config()
        print("Finished tilling")
        print("Last file filled: "+str(current_path_number))

    @classmethod
    def split(cls, path_number):
        try:
            path_string = bin(path_number)[3:]
            data = cls.read_file(path_string)
        except FileException as e:
            raise FileException("file doesn't exist")
 
        for prefix in '01':
            this_path_string = prefix+data['path_string']
            this_path_number = int("1"+this_path_string, 2)
            if not cls.file_exists(this_path_string): 
                BTC_block = BitcoinService.get_latest_block_hash()
                pub_key = config.get_pub_key(this_path_number)
                priv_key = config.get_priv_key(this_path_number)
                sig_tuple = config.sign_message(this_path_string+"_"+BTC_block+"_"+config.master_address, 1)
                file_data = {"path_string": this_path_string,
                             "path_number": this_path_number,
                             "BTC_block": BTC_block,
                             "master_address": config.master_address,
                             "sign_string": this_path_string+"_"+BTC_block+"_"+config.master_address,
                             "public_key": config.get_pub_key(1),
                             "sig": sig_tuple[0],
                             "sig_public": sig_tuple[2]}
                content_sig = config.sign_message("", 1)
                file_data['content_sig'] = content_sig[0]
                file_data['content_sig_public'] = content_sig[1]
               
                file_data['msg'] = "__".join([file_data['sign_string'],
                                              file_data['path_string'],
                                              file_data['BTC_block'],
                                              file_data['public_key'],
                                              file_data['master_address'],
                                              file_data['sig'],
                                              file_data['sig_public']
                                              ])
                file_data['hash'] = hashlib.sha256(file_data['msg'].encode('utf-8')).hexdigest()
                file_c = json.dumps(file_data)
                while len(file_c) < 65536:
                    file_c+=" "
                cls.write_file(file_data['path_string'], file_c)
       
    @classmethod
    def wipe_files(cls):
        shutil.rmtree(config.data_folder)
        config.current_path_number = 1
        config.save_config()
        print("Files wiped.")

    @classmethod
    def wipe_files_help(cls):
        print("Wipes all created data files.")

    @classmethod
    def file_exists(cls, path_string):
        try:
            cls.read_file(path_string)
            return True
        except:
            return False

    @classmethod
    def create_master_file(cls):
        try:
            os.mkdir(config.data_folder)
        except:
            pass

        if os.path.exists(config.data_folder+"/file_1.json"):
            raise errors.FileException("master file already created");
        else:
            BTC_block = BitcoinService.get_latest_block_hash()
            pub_key = config.get_pub_key(1)
            priv_key = config.get_priv_key(1)

            sig_tuple = config.sign_message(""+"_"+BTC_block+"_"+config.master_address, 1)
            if config.get_pub_key(1) != "1AT2xx3FNU9KMW6mW2iHVfuKJceJsaUPtt":
                raise BlockException("Invalid key to create root file")

            file_data = {"path_string": "",
                         "path_number": 1,
                         "BTC_block": BTC_block,
                         "master_address": config.master_address,
                         "sign_string": "_"+BTC_block+"_"+config.master_address,
                         "public_key": config.get_pub_key(1),
                         "sig": sig_tuple[0],
                         "sig_public": sig_tuple[2]}

            content_sig = config.sign_message("", 1)
            file_data['content_sig'] = content_sig[0]
            file_data['content_sig_public'] = content_sig[1]
               
            file_data['msg'] = "__".join([file_data['sign_string'],
                                          file_data['path_string'],
                                          file_data['BTC_block'],
                                          file_data['public_key'],
                                          file_data['master_address'],
                                          file_data['sig'],
                                          file_data['sig_public']])
            file_data['hash'] = hashlib.sha256(file_data['msg'].encode('utf-8')).hexdigest()

            file_c = json.dumps(file_data)
            while len(file_c) < 65536:
                file_c+=" "
            cls.write_file(file_data['path_string'], file_c)

    @classmethod
    def write_file(cls,path_string, file_content):
        dirs = list()
        tt = path_string
        while len(tt) > 8:
            dirs.append(tt[:8])
            tt = tt[8:]
        curr_d = config.data_folder
        for d in dirs:
            curr_d = curr_d+"/"+d
            try:
                os.mkdir(curr_d)
            except:
                pass
        ff = open(curr_d+"/file_1"+path_string+".json", "w")
        ff.write(file_c)

    @classmethod
    def read_file(cls, path_string):
        dirs = list()
        tt = path_string
        while len(tt) > 8:
            dirs.append(tt[:8])
            tt = tt[8:]
        curr_d = config.data_folder
        for d in dirs:
            curr_d = curr_d+"/"+d
            try:
                os.mkdir(curr_d)
            except:
                pass
        try:
            ff = open(curr_d+"/file_1"+path_string+".json")
        except:
            raise FileException()
        data = ff.read()
        p = data.partition("}")
        doc = json.loads(p[0]+"}")
        data = p[2]
        return doc

    @classmethod
    def help(cls):
        print("Show Till Help")
