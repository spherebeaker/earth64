from earth64 import config
from earth64.errors import BlockException, FileException
from earth64.services.till_service import TillService
import os
import json

class TransferService:
    @classmethod
    def get_owner(cls, path_number):
        path_string = bin(int(path_number))[3:]
        error = None
        try:
            TillService.read_file(path_string)
        except:
            error = FileException("File hasn't been created yet")

        if error:
            raise error

        public_key = config.get_pub_key(1)
        if public_key != "1AT2xx3FNU9KMW6mW2iHVfuKJceJsaUPtt":
            raise BlockException("Invalid key to determine ownership")
        #get current owner
        data = cls.read_owner(path_number)
        if data:
            for doc in data['actions']:
                print(doc)
          
            print("\nCurrent owner: "+data['actions'][-1]['new_owner'])
        else:
            print(public_key+" (root)")
     
    @classmethod
    def help_get_owner(cls):
        print("TODO")
 
    @classmethod
    def transfer(cls, path_number, address):
        public_key = config.get_pub_key(1)
        if public_key != "1AT2xx3FNU9KMW6mW2iHVfuKJceJsaUPtt":
            raise BlockException("Invalid key to transfer file ownership")
        #get current owner
        data = cls.read_owner(path_number)
        if not data:
            data['actions'] = list()
        else:
            raise BlockException("Post root transfers not supported")
        
        message = str(path_number)+"_0_"+public_key+"_"+address
        owner_sig = config.sign_message(message, 1)
        root_sig = config.sign_message(message, 1)

        doc = {"owner": public_key,
               "new_owner": address,
               "transaction_index": 0,
               "message": message,
               "owner_sig": owner_sig[0],
               "owner_sig_pub": owner_sig[2],
               "root_sig": root_sig[0],
               "root_sig_pub": root_sig[2]}
         
        data['actions'].append(doc)
        cls.write_owner(path_number, data)

    @classmethod
    def write_owner(cls, path_number, data):    
        for f in [config.data_folder, config.data_folder+"/transfers"]:
            try:
                os.mkdir(f)
            except:
                pass
        curr_d = config.data_folder+"/transfers"
        filew = open(curr_d+"/"+path_number+".sig", "w")
        filew.write(json.dumps(data))
        
    @classmethod
    def read_owner(cls, path_number):
        for f in [config.data_folder, config.data_folder+"/transfers"]:
            try:
                os.mkdir(f)
            except:
                pass
        curr_d = config.data_folder+"/transfers"
        try:
            fileo = open(curr_d+"/"+path_number+".sig")
        except:
            return {}
        data = json.loads(fileo.read())
        return data

    @classmethod
    def help_transfer(cls):
        print("TODO")
