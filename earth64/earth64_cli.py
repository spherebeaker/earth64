from earth64 import config
import sys

from earth64.services.till_service import TillService
from earth64.services.account_service import AccountService
from earth64.services.transfer_service import TransferService
from earth64.services.transaction_encoder_service import TransactionEncoderService

COMMANDS = [{"args": ["till"], "func": TillService.till, "help": TillService.help},
            {"args": ["wipe"], "func": TillService.wipe_files, "help": TillService.wipe_files_help},
            {"args": ["account", "show"], "func": AccountService.show, 
             "help": AccountService.help},
            {"args": ["account", "set", "encoded", "address"], "func": AccountService.set_encoded_address, "help": AccountService.help_set_encoded_address},
            {"args": ["transfer"], "func": TransferService.transfer, "help": TransferService.help_transfer},
            {"args": ["get", "owner"], "func": TransferService.get_owner, "help": TransferService.help_get_owner},
 
            {"args": ["message", "encode"], "func": TransactionEncoderService.encode_message_cli,
             "help": TransactionEncoderService.help_encode_message},
            {"args": ["message", "validate"], "func": TransactionEncoderService.validate_message_cli,
             "help": TransactionEncoderService.help_validate_message}           
           ]


class Earth64CLI:
    @classmethod
    def show_help(cls, help_index=None, arg_start=None):
        if help_index is not None and COMMANDS[help_index].get('help'):
            COMMANDS[help_index]['help']()
        else:
            print("Usage: till.sh COMMAND argument1 argument2 ...")
            if arg_start and arg_start in ['account']:
                print("\nPossible \""+arg_start+"\" commands:")
            else:
                print("\nPossible commands:")
            for com in COMMANDS:
                strr = " "
                for arg in com['args']:
                    strr = strr+" "+arg
                if arg_start and arg_start in ['account'] and com['args'][0] != arg_start:
                    continue
                print(strr)
            print("\nTo see a command's help page, run till.sh help COMMAND")


    @classmethod
    def parse(cls, *args):
        kwargs = dict()
        args = list()
        key = None
        keys_started = False
        if len(sys.argv) == 1:
            cls.show_help()
            return

        for arg in sys.argv[1:]:
            if key is None and arg.startswith("--"):
                key = arg[2:]
                keys_started = True
            elif key is not None and arg.startswith("--"):
                kwargs[key] = 1
                key = arg[2:]
            elif key:
                kwargs[key] = arg
                key = None
            elif keys_started is False:
                args.append(arg)
            else:
                raise ARGException("Positional argument must be before keyword arguments: "+arg)
        if key:
            kwargs[key] = 1
        chelp = False
        if args and args[0].lower() == "help":
            chelp = True
            args = args[1:]
            if len(args) == 0:
                cls.show_help()
                return
        for j, entry in enumerate(COMMANDS):
            for i, arg in enumerate(entry['args']):
                if len(args) > i and args[i] == arg:
                    continue
                break
            else:
                #run this command
                if chelp:
                    cls.show_help(j)
                    break
                else:
                    try:
                        entry['func'](*args[len(entry['args']):], **kwargs)
                    except Exception as e:
                        import traceback
                        traceback.print_exc()
                        print(e)
                        print("")
                        cls.show_help(j)
                    break
        else:
            #no matching command found... show help.
            if args and args[0]:
                cls.show_help(arg_start=args[0])


def main():
    config.load_config()   
    Earth64CLI.parse()

if __name__=='__main__':
    main()
