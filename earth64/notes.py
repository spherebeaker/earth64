"""
1. )Create Sato-server file with the name in binary [1] and time stamp it with the latest Bitcoin block hash and make it owned by a BIP32 master key (perhaps both BTC and ETH compatible public wallet address as in: 

BTC: 37vPSWmnZstZpJeM8rFzh3P2j12Er6tDTf

2. Executable code (could be Javascript) that creates Sato-Servers (files) have their names to be each point and the the leaves in the 2^64 data structure we put together (route is binary is 1 children left is binary 10 right is binary 11 etc as shown below)

(tree expand view)

3. Each and every file (Sato-Servers) will have a reserved size on disk of 64KB

4. Each Sato-Server will have the time stamp of the top root (1) and all the info from the top-root

5. The code will have as input
   a. The root number and content of the root file 
   b. The level desired to do the Tilling (initially we would insert root #1 and we would put the level to be 13 so we can generate the 1744 roots)

6. The code will have output 
   a. each and every children of the root as a Sato-Server 
   b. The root passes the ownership entitlement downwards so effectively the owner of all the children are the same as the owner of the root .
   c. The owner can sign the ownership to another public key by simply inserting singing the pubic key of the recipient and the file# and inserting the signature inside the file itself.  (while we are still setting up the Data structure this is all done centralized to setup the code) 
   d. Each and every leaf will have the entire path that lead to its creation (the binary numbers for example in the image above Sato-Server(5 ; 42) will have numbers 21, 10, 5, 2, 1
   e. Each child will have a hash of its parent at the time the child is born

7. For this MVP the files can be available in a folder for now managed by the OS where the Tilling is running If we have time in the development we would like to have this folder to have the ability to be accessible via HTTP (basically web-service publishing of some sort) 


"""
