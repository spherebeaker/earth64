# Setup python3 on your system

Ubuntu:

1.) run sudo apt-get install python3.6 python3-pip

Mac OS:

1.) run the python3.8 installer at: https://www.python.org/ftp/python/3.8.5/python-3.8.5-macosx10.9.pkg

2.) Install pip3 by downloading https://bootstrap.pypa.io/get-pip.py and running python3 get-pip.py


# To create the initial files #

1.) Install earth64 from the earth64 folder:

python3 setup.py install
pip3 install -r requirements.txt

2.) Next, compose your message for the blockchain proof:

eg: "earth64_2000"

3.) Encode the message to a bitcoin address to dust:

> sudo python3 earth64_cli.py message encode "earth64_2000"

Master public key:
1JFs83ksLKxcqncvbdM9Hf1m3dQyEkevws

Address for encoded message: 
<ENCODED ADDRESS>

3.) Send a small amount of BTC to the returned <ENCODED ADDRESS>

4.) Set the encoded address

> sudo python3 earth64_cli.py account set encoded address <ENCODED ADDRESS>

5.) Start tilling:

> sudo python3 earth64_cli.py till 

To specify a file prefix to till down from (eg: "101") use the --prefix argument.  To limit the number of levels to till down (eg: num), use the --limit argument.

> sudo python3 earth64_cli.py till --prefix 101 --limit 13

6.) To wipe all created files, run

> sudo python3 earth64_cli.py wipe
