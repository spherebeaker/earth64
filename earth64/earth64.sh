#!/bin/bash
mkdir /earth64
cd ..
../ENV3/bin/python3 setup.py install > /dev/null
../ENV3/bin/pip3 install -r requirements.txt > /dev/null
cd earth64
../../ENV3/bin/python3 earth64_cli.py $@
