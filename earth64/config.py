from bip32 import BIP32, HARDENED_INDEX
import json
import random
from cryptotools.ECDSA.secp256k1 import Message
from cryptotools.message import Signature
from cryptotools.BTC import Xprv
from cryptotools.BTC import PrivateKey, PublicKey

seed_key = None#"01"
bip_obj = None#BIP32.from_seed(bytes.fromhex(seed_key))
private_key = ""
public_key = ""

root_public_key = "1AT2xx3FNU9KMW6mW2iHVfuKJceJsaUPtt"

data_folder = "/earth64/data"
current_path_number = 1
master_address = ""

def set_master_address(add):
    global master_address
    load_config()
    master_address = add
    save_config()

def save_config():
    obj = {"data_folder": data_folder,
           "current_path_number": current_path_number,
           "seed_key": seed_key,
           "master_address": master_address}
    data = json.loads(open("config.json").read())
    for key in obj:
        data[key] = obj[key]
    ff = open("config.json", 'w')
    ff.write(json.dumps(data))

def generate_random_seed(num):
    return "".join([random.choice("0123456789abcdef") for n in range(num)])

def load_config():
    global seed_key
    global bip_obj 
    global master_address

    try:
        data = json.loads(open("config.json").read())
    except:
        data = {"seed_key": generate_random_seed(64), "master_address": master_address}
        open("config.json", "w").write(json.dumps(data))

    seed_key = data['seed_key']
    master_address = data['master_address']
    bip_obj = BIP32.from_seed(bytes.fromhex(seed_key))
 
def get_priv_key(num):
    # bip_obj.get_xpriv_from_path([1, HARDENED_INDEX, num])
    m = Xprv.from_seed(seed_key)    
    return (m/1/2147483648/num).key.hex()
 
def get_pub_key(num):
    #return bip_obj.get_xpub_from_path([1, HARDENED_INDEX, num])
    m = Xprv.from_seed(seed_key)    
    return (m/1/2147483648/num).key.to_public().to_address("P2PKH")
 
def sign_message(message, num):
    key_hex = get_priv_key(num)
    private_key = PrivateKey.from_hex(key_hex)
    public_key = private_key.to_public()
    message = Message.from_str(message)
    signature = message.sign(private_key)
    return signature.encode().hex(), public_key.to_address("P2PKH"), public_key.hex()

def verify_message(message, signature_hex, public_key_hex):
    public_key = PublicKey.from_hex(public_key_hex)
    signature = Signature.from_hex(signature_hex)
    message = Message.from_str(messag)
    return message.verify(signature, public_key)

def sign_string(string, num):
    return "TODO: sign "+string+" with key "+str(num)

def main():
    load_config()
    sign_message("hello", 1)

if __name__=='__main__':
    main()
    import pdb
    pdb.set_trace()
    
