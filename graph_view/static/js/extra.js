
function createElementFromHTML(htmlString) {
  var div = document.createElement('div');
  div.innerHTML = htmlString.trim();

  // Change this to div.childNodes to support multiple top-level nodes
  return div.firstChild; 
}


var lastMouse = {};

onmousemove = function(e){
    lastMouse.x = e.clientX;
    lastMouse.y = e.clientY;

    var elm_e1 = document.getElementById("overlay");
    var elm_e2 = document.getElementById("context");
    if (elm_e2 != undefined && elm_e2.style.display == "block") {
        //while the context menu is open, don't update here...
    }
    else if (elm_e1 != undefined) {
        //console.log(e.clientY, e.clientX);
        elm_e1.style.top = e.clientY+5+'px';
        elm_e1.style.left = e.clientX+15+'px';
    }
    


    //console.log("mouse location:", e.clientX, e.clientY)
}

function circleUpdateLoop() {
    var elms = document.getElementsByTagName("circle");
    for (var i=0;i<elms.length;i++) {
        updateCircle(elms[i]);
    }
    setTimeout(circleUpdateLoop, 200);
}

function updateCircle(elm) {
    var text = elm.nextElementSibling.children[0].innerHTML;
    var pos_string = text.substr(1, text.length)

    var elm_e = document.getElementById("overlay");
    if (elm_e == undefined) {
        var ee = createElementFromHTML("<div class=\"overlay_class\" id=\"overlay\"></div>");
        document.getElementById("root").children[0].insertAdjacentElement('beforebegin', ee);
        var elm_e = document.getElementById("overlay");
    }   

    var dd = getPosCoordinates(pos_string);// compute_block_from_pos(text);
    var ff = gData['squares'][gDataLookup[pos_string]];
    //elm_e.innerHTML = JSON.stringify(ff);
    //console.log("TTTTTT");
    //console.log(ff);
    //console.log(dd);
    //
    var topLeftCoordinate = dd['tl'];
    var bottomRightCoordinate = dd['br'];
    var validNode = 0;
    function validPoint(point) {
	if (point[0] <= 90 && point[0] >= -90 && point[1] <= 180 && point[1]>= -180) {
		return true;
	}
	return false;
    }

    var coords = [ [dd['tl']['lat'], dd['tl']['lon']],
		   [dd['br']['lat'], dd['tl']['lon']],
		   [dd['br']['lat'], dd['br']['lon']],
		   [dd['tl']['lat'], dd['br']['lon']],
                 ]
    for (var i=0;i<4;i++) {
        if (validPoint(coords[i]) == false)
          validNode+=1;
    }
    if (validNode >= 4)
	validNode = 0;
    else if (validNode == 0)
	validNode = 2;
    else
	validNode = 1;
    if (ff)
        var height = ff['h'];
    else
        var height = pos_string.length-1;
    var isNFT = true;
    if (height != 12) 
        isNFT = false
    var isSubNFT = true;
    if (height <= 12)
        isSubNFT = false;

    elm.onmouseenter = function(e) {
        var elm2 = document.getElementById("context");
        if (elm2 && elm2.style.display == "block")
            return;
        elm_e.style.display = "block";
        //change text here

        elm_e.innerHTML = "<b>Top Left Coordinate</b><br>\n"+
                          " lat: "+dd['tl']['lat']+'  lon: '+dd['tl']['lon']+'<br>\n'+
                          "<b>Bottom Right Coordinate</b><br>\n"+
                          " lat: "+dd['br']['lat']+"  lon: "+dd['br']['lon']+'<br>\n';
	if (validNode == 0) {
		elm_e.style.background = "#B81D13";
	}
	else if (validNode == 1) {
		elm_e.style.background = "#EFB700";
	}
	else if (validNode == 2) {
		elm_e.style.background = "#008450";
	}
        
    }

    elm.onmouseleave = function(e) {
        elm_e.style.display = "None";
    }

    elm.oncontextmenu = function(e) {
        e.preventDefault();
        var url = "map.html?tl="+topLeftCoordinate['lat']+','+topLeftCoordinate['lon']+"&br="+bottomRightCoordinate['lat']+','+bottomRightCoordinate['lon'];
        if (validNode == 2) {
            showContextMenu(pos_string);    
	} else if (validNode == 1) {
            alert("Virtual node");
	} else if (validNode == 0) {
            alert("Invalid node");
	}

        
    }
}

function showContextMenu(pos_string) {
    var dd = getPosCoordinates(pos_string);

    var topLeftCoordinate = dd['tl'];
    var bottomRightCoordinate = dd['br'];

    var elm_e = document.getElementById("context");
    if (elm_e == undefined) {
        var ee = createElementFromHTML("<div class=\"context_class\" id=\"context\"></div>");
        document.getElementById("root").children[0].insertAdjacentElement('beforebegin', ee);
        var elm_e = document.getElementById("context");

        elm_e.onclick = function(event){
            event.stopPropagation();
        }
        document.getElementById("root").onclick=function(event) {
            document.getElementById("context").style.display="none";
        }
        document.getElementById("cancel_overlay").onmousedown = function() {
            document.getElementById("context").style.display="none";
            document.getElementById("cancel_overlay").style.display="none";
        }

    }

    var url = "map.html?tl="+topLeftCoordinate['lat']+','+topLeftCoordinate['lon']+"&br="+bottomRightCoordinate['lat']+','+bottomRightCoordinate['lon'];
    var isSatoServer = false;

    if (isSatoServer == false) {
        var innerNodes = "<b>Options:</b><br><div class=\"contextElm\" onclick=\"createSatoServer('"+pos_string+"');\">Create Sato-Server</div>";
    } else {
        var innerNodes = "<div onclick=\"showSatoServer('"+pos_string+"');\">Show Sato-Server</div>";
    }
    innerNodes+= "<div class=\"contextElm\" onclick=\"openMap('"+url+"');\">View Coordinates</div>"
 
    elm_e.innerHTML = innerNodes;
    elm_e.style.display="block";

    var elm_overlay = document.getElementById("overlay");
    if (elm_overlay != undefined)
        elm_overlay.style.display = 'none';

    document.getElementById("cancel_overlay").style.display="block";
        
    elm_e.style.top = lastMouse.y+5+'px';
    elm_e.style.left = lastMouse.x+15+'px';
}

function openMap(url) {
    window.open(url, '_blank').focus();
}


var gDataLookup = {};

function initGData() {
    //console.log("RRRRRR");
    for (var i=0;i<gData['squares'].length;i++) {
        if (gData['squares'][i]['pos'] != undefined) {
            gDataLookup[gData['squares'][i]['pos']] = i;
            //console.log("DDDDDDDDDDDDDDDDDDD");
        }
    }
    circleUpdateLoop();
}


function getPosStringFromCoords() {
    var lat = document.getElementById("lat").value;
    var lon = document.getElementById("lon").value;
    lat = parseFloat(lat);
    lon = parseFloat(lon);
    //console.log(lat,lon);
    var output = compute_block_from_coords(lat, lon, 13);
    var pos = '1'+output['pos'];
    return pos;
}


function updatePosString(){
    var pos = getPosStringFromCoords(); 
    document.getElementById("posString").innerHTML = "Position string: "+pos;
}

function goToCoords() {
    var pos_string = getPosStringFromCoords();    
    var dd = getPosCoordinates(pos_string.substring(1, pos_string.length-1));
    var topLeftCoordinate = dd['tl'];
    var bottomRightCoordinate = dd['br'];

    var url = "map.html?tl="+topLeftCoordinate['lat']+','+topLeftCoordinate['lon']+"&br="+bottomRightCoordinate['lat']+','+bottomRightCoordinate['lon'];
       
    window.open(url, '_blank').focus();
}



setTimeout(initGData,2000);
