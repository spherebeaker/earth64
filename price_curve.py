
def price_curve(n):
    if n < 232:
        return int(1000000*(1.01**n))/100
    elif n< 694:
        val = (1.01**232)
        val = val*(1.005**(n-232))
        return int(val*1000000)/100
    elif n<1615:
        val = (1.01**232)
        val = val*(1.005**(694-232))
        val = val*(1.0025**(n-694))
        return int(val*1000000)/100
    else:
        val = (1.01**232)
        val = val*(1.005**(694-232))
        val = val*(1.0025**(1615-694))
        val = val*(1.00125**(n-1615))
        return int(val*1000000)/100

def price_curve_discount(n):
    val = price_curve(n)
    if n < 29:
        discounted = int(100*(val*0.40))/100
        return discounted
    elif n < 89:
        discounted = int(100*(val*0.50))/100
        return discounted
    elif n < 232:
        discounted = int(100*(val*0.60))/100
        return discounted
    else:
        discounted = val
        return discounted
   
def main():
    print("NFT#,Price,Discount")
    for i in range(1701):
       print(str(i+1)+","+str(price_curve(i))+","+str(price_curve_discount(i)))

if __name__=='__main__':
    main()
