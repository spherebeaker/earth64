from setuptools import setup, find_packages

__VERSION__ = "0.0.1"

def main(args=None):
    with open("readme.md", "r") as fh:
        long_description = fh.read()
    setup_required_packages = []
    required_packages =  ["bip32", "coinaddress", "tornado>5.1", "ecdsa",
    ]

    test_required_packages = ["nose", "coverage"]
    settings = dict(name="earth64",
                    version = __VERSION__,
                    long_description=long_description,
                    author="",
                    author_email="",
                    description="Earth64 POC",
                    long_description_content_type="text/markdown",
                    url="",
                    packages=find_packages(),
                    include_package_data=True,
                    zip_safe=False,
                    install_requires=required_packages,
                    tests_require=test_required_packages,
                    test_suite="nose.collector",
                    setup_requires=setup_required_packages,
                    entry_points="""\
                        [console_scripts]
                        earth64=earth64.till:main
                        """,
                    classifiers=[
                        "Programming Language :: Python :: 3",
                        "License :: OSI Approved :: MIT License",
                        "Operating System :: OS Independent",
                    ],
               )
    
    if args:
        settings['script_name'] = __file__
        settings['script_args'] = args
    setup(**settings)

if __name__=='__main__':
    main()


