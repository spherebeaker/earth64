# Ocean32
Ocean32 NFT


# Instructions

- Clone Repository

```
git clone https://github.com/https://github.com/BitcoinBay/Ocean32.git && cd Ocean32
```
- Install Hardhat

```
npm install --save-dev hardhat
```

- Install Openzeppelin hardhat-upgrades

```
npm install @openzeppelin/hardhat-upgrades
```

- compile contracts

```
npx hardhat compile
```
-
