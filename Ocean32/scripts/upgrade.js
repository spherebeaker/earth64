// scripts/upgrade_box.js
const { ethers, upgrades } = require("hardhat");

async function main() {
  const EARTH64 = await ethers.getContractFactory("Earth64");

  const EARTH64V2 = await ethers.getContractFactory("Earth64V2");
  console.log("Upgrading EARTH64...");

  // paste the V1 address into the ""
  const earth64V2 = await upgrades.upgradeProxy("0xcC4A24725ebBd5c10Fd30d7f5C1870f09B92189D", EARTH64V2);
  console.log("EARTH64 upgraded");
}

main();
