// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const hre = require("hardhat");

async function main() {
   const EARTH64 = await ethers.getContractFactory("Earth64");
   const earth64 = await upgrades.deployProxy(EARTH64,{kind:"uups"});
   await earth64.deployed();
   console.log("Earth64 deployed to:", earth64.address); 
 
   /*  
   const REVSHARETOKEN = await ethers.getContractFactory("RevShareToken");
   const revsharetoken = await upgrades.deployProxy(REVSHARETOKEN,{kind:"uups"});
   await revsharetoken.deployed();

   console.log("RevShareToken deployed to:", revsharetoken.address);
   */
   /*
   //const earth64_address = "0xbf6D7aeB92875d989bf25f84FCd0b62fb4595c64";
   const earth64_address = "0xd1bBeCf0cA86488ce16ea85Bd1211a366A3D1f53";//exisiting rinkeby
   const EARTH64V2 = await ethers.getContractFactory("Earth64V1_5");
   console.log("Upgrading Earth64... to V1_5");
   const earth64V2 = await upgrades.upgradeProxy(earth64_address, EARTH64V2);
   console.log("Earth64 upgraded");
   */
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
