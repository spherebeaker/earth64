// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";


//Eth Price Oracle, Chainlink Service
interface EPO{
  function latestAnswer() external view returns(uint);
}

contract RevShareToken is ERC20Upgradeable, UUPSUpgradeable, OwnableUpgradeable {
    uint256 private _maxSupply;
 
    mapping(address=>bool) _isOwner;
    uint _ownersLength;
    address[] _owners;
    address withdrawAddress;
 
    address EPOAddress; //Chainlink's free Eth Price Oracle
    bool isTestnet;
    uint maxMintable;
 
    function initialize() initializer public {
        ERC20Upgradeable.__ERC20_init("ShareEarth64", "SE64");
        EPOAddress = 0x5f4eC3Df9cbd43714FE2740f5E3616155c5b8419;
        _maxSupply = 2**40;//100000000000;
        maxMintable = uint((2**40)/uint(5));//20%
        withdrawAddress = 0x7dA2BF83041d05323553f4F3A3a9e27aF0bD9250;
        isTestnet = true;
    }

    function getMintedSupply() public view virtual returns (uint) {
        return maxMintable;
    }
    function getMaxSupply() public view virtual returns (uint) {
        return _maxSupply;
    }

    function decimals() public view virtual override returns (uint8) {
        return 10;//0;
    }

    function _transfer(
        address sender,
        address recipient,
        uint256 amount
    ) internal virtual override {
        ERC20Upgradeable._transfer(sender, recipient, amount);

        if (_isOwner[recipient] == false) {
            _isOwner[recipient] = true;
            _owners.push(recipient);
            _ownersLength+=1;
        }
    }

    function _mint(address account, uint256 amount) internal virtual override {
        require(amount+ERC20Upgradeable.totalSupply() <= maxMintable, "Too many tokens minted.");
        require(amount+ERC20Upgradeable.totalSupply() <= _maxSupply, "Too many tokens minted.");
        ERC20Upgradeable._mint(account, amount);
        if (_isOwner[account] == false) {
            _isOwner[account] = true;
            _owners.push(account);
            _ownersLength+=1;
        }
    }

    receive() external payable virtual {
        uint PriceUSD = 10000000000000000;//100mil for 100%

        //eth price per share
        uint Price = getPriceInWei(PriceUSD)/(_maxSupply);
        uint shares = msg.value/Price;
        
        _mint(msg.sender, shares);
    }

    function withdraw() public virtual {
        require(msg.sender == withdrawAddress, "Only withdrawAddress can withdraw");
        payable(withdrawAddress).transfer(address(this).balance);
    }

    function getPriceInWei(uint256 price) public view virtual returns(uint){
      return ((10**18)*price)/getETHPriceInUSD();
    }

    function getETHPriceInUSD() public view returns (uint){
      if (isTestnet) {
        return 252952000000000;//252952000000
      }
      else {
        return EPO(EPOAddress).latestAnswer();
      }
    }




    function _authorizeUpgrade(address newImplementation) internal override onlyOwner
    {
    }


    function getOwners() view public virtual returns(address[] memory) {
        return _owners;
    }

    function getOwnersLength() view public virtual returns(uint) {
        return _ownersLength;
    }
}
   
