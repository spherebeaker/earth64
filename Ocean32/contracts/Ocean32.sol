// SPDX-License-Identifier: MIT

pragma solidity ^0.8.2;

import "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "./math/PriceCurve.sol";


//Eth Price Oracle, Chainlink Service
interface EPO{
  function latestAnswer() external view returns(uint);
}

//Upgradeable ERC721 Contract, using Openzeppelin's UUPS upgradeable pattern
contract Ocean32 is ERC721Upgradeable, UUPSUpgradeable, OwnableUpgradeable, PriceCurve{

  uint public count;

  address EPOAddress; //Chainlink's free Eth Price Oracle
  uint public lastPurchase; //Date of last purchase
  uint public lastPrice;

  function initialize() initializer public {
      __ERC721_init("Ocean32","O32");
      __Ownable_init(); //Initialize Owner of Contract as deployer
      EPOAddress = 0x5f4eC3Df9cbd43714FE2740f5E3616155c5b8419;
      count = 0;
      lastPurchase = 0;
      lastPrice = 1000000000000;
  }

  function purhcase() public payable{
    uint Price = getNFTPriceInWei();
    require(msg.value>=Price, "Not enough ETH was provided");
    require(block.timestamp>=(lastPurchase+(1 days)), "Only one purchase allowed every 24h");
    payable(msg.sender).transfer(msg.value-Price);
    _safeMint(msg.sender,count,"");
    count++;
    lastPurchase = block.timestamp;
  }

  function getNFTPriceInWei() public view returns(uint){
    return ((10**18)*getOCEAN32PriceInUSD(count))/getETHPriceInUSD();
  }

  receive() external payable {
    _safeMint(msg.sender,count,"");
  }

  function getETHPriceInUSD() public view returns (uint){
    return EPO(EPOAddress).latestAnswer();
  }

  function withdraw() onlyOwner public{
    payable(owner()).transfer(address(this).balance);
  }


  function _authorizeUpgrade(address newImplementation) internal override onlyOwner
  {
  }

  /* function mint(address to, uint256 id, uint256 amount, bytes memory data) public virtual {
    _mint(to, id, amount, data);
  }

  function mintBatch(address to, uint256[] memory ids, uint256[] memory amounts, bytes memory data) public virtual {
    _mintBatch(to, ids, amounts, data);
  } */
}

contract Ocean32V2 is Ocean32 {
  uint public newValue;

  function versionCheck() pure public returns (string memory) {
    return "Ocean32V2";
  }

  function incrementNewValue() public {
    newValue++;
  }
}
