// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

contract PriceCurve {
    uint256 constant oneUSD = 10**8;
    uint256 constant MAX_CURVE = 1700;
    uint256 constant TIER0 = (10000 * oneUSD);
    uint256 constant TIER1 = (35000 * oneUSD);
    uint256 constant TIER2 = (85000 * oneUSD);
    uint256 constant TIER3 = (185000 * oneUSD);
    uint256 constant TIER4 = (985000 * oneUSD);
    uint256 constant TIER5 = (1705000 * oneUSD);


    // Price curve split into pieces. Can be modified.
    function getOCEAN32PriceInUSD(uint256 n) public pure returns (uint256 price) {
        require(n <= MAX_CURVE, 'cannot pass MAX_CURVE');

        if (n <= 250) {
            return TIER0 +(100 * n * oneUSD);
        }
        if (n > 250 && n <= 500) {
            return TIER1 + (200 * (n - 250) * oneUSD);
        }
        if (n > 500 && n <= 750) {
            return TIER2 + (400 * (n - 500) * oneUSD);
        }
        if (n > 750 && n <= 1250) {
            return TIER3 + (800 * (n - 750) * oneUSD);
        }
        if (n > 1250 && n <= 1700) {
            return TIER4 + (1600 * (n - 1250) * oneUSD);
        }
        //Last NFT is number 1700
        //number 1700 costs 450*1600 + 985000 = 1705000 USD
        if (n == 1700) {
            return 0;
        }

        // Keeping the compiler happy.
        // Should never get here.
        return TIER5;
    }
}
