// SPDX-License-Identifier: MIT

pragma solidity ^0.8.2;

import "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";


//Eth Price Oracle, Chainlink Service
interface EPO{
  function latestAnswer() external view returns(uint);
}

//Upgradeable ERC721 Contract, using Openzeppelin's UUPS upgradeable pattern
contract Ocean32 is ERC721Upgradeable, UUPSUpgradeable, OwnableUpgradeable {

  uint public count;

  address EPOAddress; //Chainlink's free Eth Price Oracle
  uint public lastPurchase; //Date of last purchase
  uint public lastPrice;
  string public ipfsListData;
  uint public deployTime;
  mapping(uint256 => string) public burnData;

  function initialize() initializer public {
      __ERC721_init("Ocean32","O32");
      __Ownable_init(); //Initialize Owner of Contract as deployer
      EPOAddress = 0x5f4eC3Df9cbd43714FE2740f5E3616155c5b8419;
      ipfsListData = "https://ipfs.io/ipfs/QmQ7DfF1RyVH6tfdtS4e3Go2zCgnnH5XPiRC4npVnsTKsv?filename=Ocean32TokenList.csv";
      count = 0;
      lastPurchase = 0;
      lastPrice = 1;//1000000000000;
      deployTime = block.timestamp;
  }

  function getNextPrice() public view returns(uint256){
      if (count == 0) {
          return lastPrice;
      }
      else if (count < 232) {
          return lastPrice + lastPrice/100;
      }
      else if (count < 694) {
          return lastPrice + lastPrice/200;
      }
      else if (count < 1615) {
          return lastPrice + lastPrice/400;
      }
      else {
          return lastPrice + lastPrice/800;
      }
  }

  function applyDiscount(uint256 amount) public view returns(uint256) {
    if (block.timestamp <= deployTime+(1 days)) {//(180 * 1 days)) {
      return (amount*60)/100;
    }
    return amount;
  }

  function getNextPriceEth() public view returns(uint256) {
    return applyDiscount(getNFTPriceInWei(getNextPrice()));
  }

  function purchase() public payable{
    uint PriceUSD = getNextPrice();
    uint Price = applyDiscount(getNFTPriceInWei(PriceUSD));
    require(count < 1701, "Maximum number of tokens already minted");
    require(msg.value>=Price, "Not enough ETH was provided");
    require(block.timestamp>=(lastPurchase+(60)), "One one purchase allowed every 60 seconds");// 1 days)), "Only one purchase allowed every 24h");
    payable(msg.sender).transfer(msg.value-Price);
    _safeMint(msg.sender,count,"");
    count++;
    lastPurchase = block.timestamp;
    lastPrice = PriceUSD;
  }

  function getNFTPriceInWei(uint256 price) public view returns(uint){
    return ((10**18)*price)/getETHPriceInUSD();
  }

  receive() external payable {
    purchase();
  }

  function getETHPriceInUSD() public view returns (uint){
    return 252952000000;//EPO(EPOAddress).latestAnswer();
  }

  function withdraw() onlyOwner public{
    payable(owner()).transfer(address(this).balance);
  }


  function burnTo(uint256 tokenId, string memory burnString) public {
    address owner = ownerOf(tokenId);
    require(owner == msg.sender, "ERC721: must own token to burn it");
    burnData[tokenId] = burnString;
    _burn(tokenId);
  }

  function getBurnInfo(uint256 tokenId) public view returns(string memory){
    return burnData[tokenId];
  }

  function _authorizeUpgrade(address newImplementation) internal override onlyOwner
  {
  }

  /* function mint(address to, uint256 id, uint256 amount, bytes memory data) public virtual {
    _mint(to, id, amount, data);
  }

  function mintBatch(address to, uint256[] memory ids, uint256[] memory amounts, bytes memory data) public virtual {
    _mintBatch(to, ids, amounts, data);
  } */
}

contract Ocean32V2 is Ocean32 {
  uint public newValue;

  function versionCheck() pure public returns (string memory) {
    return "Ocean32V2";
  }

  function incrementNewValue() public {
    newValue++;
  }
}
