// SPDX-License-Identifier: MIT

pragma solidity ^0.8.2;

import "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";


//Eth Price Oracle, Chainlink Service
interface EPO{
  function latestAnswer() external view returns(uint);
}

interface ShareToken {
  function totalSupply() external view returns(uint);
  function getMaxSupply() external view returns(uint);

  function balanceOf(address account) external view returns(uint256);
  function getOwners() external view returns(address[] memory);
  function getOwnersLength() external view returns(uint);
}

//Upgradeable ERC721 Contract, using Openzeppelin's UUPS upgradeable pattern
contract Earth64V2 is ERC721Upgradeable, UUPSUpgradeable, OwnableUpgradeable {
  /* V1 variables */
  uint32 public count;

  address EPOAddress; //Chainlink's free Eth Price Oracle
  address withdrawAddress;
  uint public lastPurchase; //Date of last purchase
  uint public lastPrice;
  string public ipfsListData;
  uint public deployTime;
  mapping(uint256 => string) public burnData;
  uint32 public totalSupply;
  uint public timePerToken;
  uint public postListStartAmount;
  bool private isTestnet;

  /* V2 variables */
  bool public v2Inited;
  address public sharesToken;
  uint reservedBalance;
  mapping(address => uint) public revBalances;
  uint lastRevSharePayout;
  uint timePerPayout;

  function initialize() initializer public {
      /*
      __ERC721_init("Earth64","E64");
      __Ownable_init(); //Initialize Owner of Contract as deployer
      EPOAddress = 0x5f4eC3Df9cbd43714FE2740f5E3616155c5b8419;
      ipfsListData = "https://ipfs.io/ipfs/QmXJiddioQaUg6TWjZgFauw2fYdoMxXjx6jeBhn42SqeMo?filename=ImmmutableList1.csv";
      count = 0;
      lastPurchase = 0;
      totalSupply = 1701;
   
      isTestnet = true;
      if (isTestnet) {      
        lastPrice = 100000000;
        postListStartAmount = 1000000000;
        timePerToken = 60;
        withdrawAddress = 0xEe3F7346d100FF28648c4Ff2c551FdF58E64c8bb;
      }
      else {
        lastPrice = 1000000000000;
        postListStartAmount = 3267710000000;
        timePerToken = 1 days;
        withdrawAddress = 0xE4814c81E20b3740C88955e0A1B3f677C848Ad96;
      }
      deployTime = block.timestamp;
      */
  }

  function getNextPrice() public view virtual returns(uint256){
      if (count == 0) {
          return lastPrice;
      }
      else if (count < 232) {
          return lastPrice + lastPrice/100;
      }
      else if (count < 694) {
          return lastPrice + lastPrice/200;
      }
      else if (count < 1615) {
          return lastPrice + lastPrice/400;
      }
      else {
          return lastPrice + lastPrice/800;
      }
  }

  function applyDiscount(uint256 amount) public view virtual returns(uint256) {
    if (count < 120) {
      //The first 120 tokens are pre-set to be $32,677.10
      return getNFTPriceInWei(postListStartAmount);
    }
    return amount;
  }

  function getNextPriceEth() public view virtual returns(uint256) {
    return applyDiscount(getNFTPriceInWei(getNextPrice()));
  }

  function nextPurchaseTime() public view virtual returns(uint256) {
    return lastPurchase+timePerToken;
  }

  function getReservedBalance() public view virtual returns(uint256) {
    return reservedBalance;
  }

  function purchase() public payable virtual {
    uint PriceUSD = getNextPrice();
    uint Price = applyDiscount(getNFTPriceInWei(PriceUSD));

    uint revShareStage1 = 250;
    uint revShareStage2 = 500;
    if (isTestnet) {
        revShareStage1 = 10;
        revShareStage2 = 20;
    }

    require(block.timestamp>=(lastPurchase+timePerToken), "Only one purchase allowed every 24 hours.");
    require(count < 1701, "Maximum number of tokens already minted");

    if (count == 0) {
      Price = Price/100;
    }
    require(msg.value>=Price, "Not enough ETH was provided");
    payable(msg.sender).transfer(msg.value-Price);

    if (count >= revShareStage1 && count < revShareStage2 && count%2 == 0) {
        uint arrayLength = ShareToken(sharesToken).getOwnersLength();
        uint revMaxSupply = ShareToken(sharesToken).getMaxSupply();

        for (uint i=0; i<arrayLength; i++) {
          address owner = ShareToken(sharesToken).getOwners()[i];
          uint shares = ShareToken(sharesToken).balanceOf(owner);
          revBalances[owner]+=2*(Price)*shares/revMaxSupply;
          reservedBalance += 2*(Price)*shares/revMaxSupply;
        }
    }
    else if (count >= revShareStage2) {
        uint arrayLength = ShareToken(sharesToken).getOwnersLength();
        uint revMaxSupply = ShareToken(sharesToken).getMaxSupply();

        for (uint i=0; i<arrayLength; i++) {
          address owner = ShareToken(sharesToken).getOwners()[i];
          uint shares = ShareToken(sharesToken).balanceOf(owner);
          revBalances[owner]+= (Price)*shares/revMaxSupply;
          reservedBalance += (Price)*shares/revMaxSupply;
        }
    }

    _safeMint(msg.sender,count,"");
    count++;
    lastPurchase = block.timestamp;
    lastPrice = PriceUSD;
  }

  function getNFTPriceInWei(uint256 price) public view virtual returns(uint){
    return ((10**18)*price)/getETHPriceInUSD();
  }

  receive() external payable virtual {
    purchase();
  }

  function getETHPriceInUSD() public view virtual returns(uint){
    if (isTestnet) {
      return 252952000000;
    }
    else {
      return EPO(EPOAddress).latestAnswer();
    }
  }

  function withdraw() public virtual {
    require(msg.sender == withdrawAddress, "Only withdrawAddress can withdraw");
    require(address(this).balance > reservedBalance, "Not enough balance.");
    payable(withdrawAddress).transfer(address(this).balance-reservedBalance);
  }


  function burnTo(uint256 tokenId, string memory burnString) public virtual {
    address owner = ownerOf(tokenId);
    require(owner == msg.sender, "ERC721: must own token to burn it");
    burnData[tokenId] = burnString;
    _burn(tokenId);
  }

  function getBurnInfo(uint256 tokenId) public view virtual returns(string memory){
    return burnData[tokenId];
  }

  function _authorizeUpgrade(address newImplementation) internal override onlyOwner
  {
  }

  /* function mint(address to, uint256 id, uint256 amount, bytes memory data) public virtual {
    _mint(to, id, amount, data);
  }

  function mintBatch(address to, uint256[] memory ids, uint256[] memory amounts, bytes memory data) public virtual {
    _mintBatch(to, ids, amounts, data);
  } */

  function revSharePayout() public virtual {
    if (isTestnet)
        timePerPayout = 5*60;
    require(block.timestamp >= lastRevSharePayout+timePerPayout, "Too soon since last payout.");
    require(address(this).balance >= reservedBalance, "Not enough balance.");

    uint arrayLength = ShareToken(sharesToken).getOwnersLength();

    for (uint i=0; i<arrayLength; i++) {
        address owner = ShareToken(sharesToken).getOwners()[i];
        if (revBalances[owner] > 0) {
            payable(owner).transfer(revBalances[owner]);
            revBalances[owner] = 0;
        }
    }
    reservedBalance = 0;
    lastRevSharePayout = block.timestamp;
  }

  function getLastRevSharePayout() public virtual returns(uint){
    return lastRevSharePayout;
  }

  function getTimePerPayout() public virtual returns(uint){
    return timePerPayout;
  }

  function v2Initialize() public {  
    //require(v2Inited == false, "V2 already Inited.");
    //sharesToken = 0x5f4eC3Df9cbd43714FE2740f5E3616155c5b8419;
    //sharesToken = 0x5FC8d32690cc91D4c39d9d3abcBD16989F875707;
    sharesToken = 0x963834dE57737bE6EA8bd754A778C2826E2d4980;
    v2Inited = true;
    if (isTestnet) {
        timePerPayout = 60*5;//roughly 5 minutes
    }
    else {
        timePerPayout = 60*60*24*28;//roughly a month
    }
  }
}

