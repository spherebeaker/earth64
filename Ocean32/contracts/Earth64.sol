// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";


//Eth Price Oracle, Chainlink Service
interface EPO{
  function latestAnswer() external view returns(uint);
}

//Upgradeable ERC721 Contract, using Openzeppelin's UUPS upgradeable pattern
contract Earth64 is ERC721Upgradeable, UUPSUpgradeable, OwnableUpgradeable {
  uint32 public count;

  address EPOAddress; //Chainlink's free Eth Price Oracle
  address withdrawAddress;
  uint public lastPurchase; //Date of last purchase
  uint public lastPrice;
  string public ipfsListData;
  uint public deployTime;
  mapping(uint256 => string) public burnData;
  uint32 public totalSupply;
  uint public timePerToken;
  uint public postListStartAmount;
  bool private isTestnet;

  function initialize() initializer public {
      __ERC721_init("Earth64","E64");
      __Ownable_init(); //Initialize Owner of Contract as deployer
      EPOAddress = 0x5f4eC3Df9cbd43714FE2740f5E3616155c5b8419;
      ipfsListData = "https://ipfs.io/ipfs/QmXJiddioQaUg6TWjZgFauw2fYdoMxXjx6jeBhn42SqeMo?filename=ImmmutableList1.csv";
      count = 0;
      lastPurchase = 0;
      totalSupply = 1701;
   
      isTestnet = true;
      if (isTestnet) {      
        lastPrice = 100000000;
        postListStartAmount = 1000000000;
        timePerToken = 60;
        withdrawAddress = 0xEe3F7346d100FF28648c4Ff2c551FdF58E64c8bb;
      }
      else {
        lastPrice = 1000000000000;
        postListStartAmount = 3267710000000;
        timePerToken = 1 days;
        withdrawAddress = 0xE4814c81E20b3740C88955e0A1B3f677C848Ad96;
      }
      deployTime = block.timestamp;
  }

  function getNextPrice() public view returns(uint256){
      if (count == 0) {
          return lastPrice;
      }
      else if (count < 232) {
          return lastPrice + lastPrice/100;
      }
      else if (count < 694) {
          return lastPrice + lastPrice/200;
      }
      else if (count < 1615) {
          return lastPrice + lastPrice/400;
      }
      else {
          return lastPrice + lastPrice/800;
      }
  }

  function applyDiscount(uint256 amount) public view returns(uint256) {
    if (count < 120) {
      //The first 120 tokens are pre-set to be $32,677.10
      return getNFTPriceInWei(postListStartAmount);
    }
    return amount;
  }

  function getNextPriceEth() public view returns(uint256) {
    return applyDiscount(getNFTPriceInWei(getNextPrice()));
  }

  function nextPurchaseTime() public view returns(uint256) {
    return lastPurchase+timePerToken;
  }

  function purchase() public payable {
    uint PriceUSD = getNextPrice();
    uint Price = applyDiscount(getNFTPriceInWei(PriceUSD));

    require(block.timestamp>=(lastPurchase+timePerToken), "Only one purchase allowed every 24 hours.");
    require(count < 1701, "Maximum number of tokens already minted");
 
    if (count == 0) {
      Price = Price/100;
    }
    require(msg.value>=Price, "Not enough ETH was provided");
    payable(msg.sender).transfer(msg.value-Price);

    _safeMint(msg.sender,count,"");
    count++;
    lastPurchase = block.timestamp;
    lastPrice = PriceUSD;
  }

  function getNFTPriceInWei(uint256 price) public view returns(uint){
    return ((10**18)*price)/getETHPriceInUSD();
  }

  receive() external payable {
    purchase();
  }

  function getETHPriceInUSD() public view returns (uint){
    if (isTestnet) {
      return 252952000000;
    }
    else {
      return EPO(EPOAddress).latestAnswer();
    }
  }

  function withdraw() public {
    require(msg.sender == withdrawAddress, "Only withdrawAddress can withdraw");
    payable(withdrawAddress).transfer(address(this).balance);
  }


  function burnTo(uint256 tokenId, string memory burnString) public {
    address owner = ownerOf(tokenId);
    require(owner == msg.sender, "ERC721: must own token to burn it");
    burnData[tokenId] = burnString;
    _burn(tokenId);
  }

  function getBurnInfo(uint256 tokenId) public view returns(string memory){
    return burnData[tokenId];
  }

  function _authorizeUpgrade(address newImplementation) internal override onlyOwner
  {
  }

  /* function mint(address to, uint256 id, uint256 amount, bytes memory data) public virtual {
    _mint(to, id, amount, data);
  }

  function mintBatch(address to, uint256[] memory ids, uint256[] memory amounts, bytes memory data) public virtual {
    _mintBatch(to, ids, amounts, data);
  } */
}

