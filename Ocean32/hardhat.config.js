require("@nomiclabs/hardhat-waffle");
require('@nomiclabs/hardhat-ethers');
require('@openzeppelin/hardhat-upgrades');

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
 module.exports = {
   defaultNetwork: "rinkeby",

       networks: {
         hardhat: {
         },

      mainnet: {
        url: "https://eth-mainnet.alchemyapi.io/v2/zJxBAqhdPyE6XJRWAcw17LD9uHo8ifdJ",
        accounts: []
      },

      rinkeby: {
         url: "https://eth-rinkeby.alchemyapi.io/v2/Nj4dz0-UQClz9_FdK9JJkprxELVSMcc_",
        accounts: ["0x73f3093aebc59830b386428814ff4bfc0c13af806564a15e3829a683e4d63f20"]
       },
       ropsten: {
         url: "https://eth-ropsten.alchemyapi.io/v2/GhFikNm5T3chbBNnPuUW_paHt6jHqnQD",
         accounts: ["0x7afb85067473a0738ef13afccbbbf45b432a7319c4b1b5665128d2e9c90b1778"]
       },
       goerli: {
         url: "https://eth-goerli.alchemyapi.io/v2/43sWHEmZ8YfC6MwPv8U37zJTCKzr_TSl",
         accounts: ["0x368afb2b011a055d00370b3b3e52a678fb3e128dacd85c6ec045054aaf7c2fbc"]
       }
       },

     solidity: {
       version: "0.8.4"
     },

     paths: {
       sources: "./contracts",
       tests: "./test",
       cache: "./cache",
       artifacts: "./artifacts"
     },

     mocha: {
       timeout: 20000
   }
 };
